package app.sum.bti.config;

import app.sum.bti.login.LoginCheckInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.PathResourceResolver;

@Configuration
public class WebServletConfig implements WebMvcConfigurer {

    @Value("${server.stored.img.path}")
    private String imagePath;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/img/**") // 파일경로는 뭐가 오든지 상관없음!
                .addResourceLocations("file:///"+imagePath)      // 이 물리적인 경로(외부경로)에 있는 것을 하나 위 줄의 이름으로 쓰겠다!
                .setCachePeriod(0)                               // 이 리소스에대한 요청이 유지되는 시간 0 캐시 사용안하겠다!
                .resourceChain(true)
                .addResolver(new PathResourceResolver());
    }

    //인터셉터 등록
    @Override
    public void addInterceptors(InterceptorRegistry registry){
        registry.addInterceptor(new LoginCheckInterceptor())
                .addPathPatterns("/home/**")        // 메인페이지 접근시
                .addPathPatterns("/couple/**")      // 커플존    접근시
                .addPathPatterns("/friend/**")      // 프랜드존  접근시
                .addPathPatterns("/postBox/**");    // 쪽지존    접근시
    }
}
