package app.sum.bti.login.mapper;

import app.sum.bti.login.vo.LoginVO;
import org.apache.ibatis.annotations.Mapper;

import java.sql.SQLException;
import java.util.Map;

@Mapper
public interface LoginMapper {

  
    // 사용자 정보 가져오기
    public LoginVO.LoginUserInfo getLoginUserInfo(LoginVO.LoginInfo params) throws SQLException;

}
