package app.sum.bti.login.service;

import app.sum.bti.login.mapper.LoginMapper;
import app.sum.bti.login.vo.LoginVO;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class LoginService {

    private final LoginMapper mapper;

    private final BCryptPasswordEncoder passwordEncoder;

    // 사용자 정보 가져오기
    public LoginVO.LoginUserInfo getLoginUserInfo (LoginVO.LoginInfo params) throws SQLException{

        LoginVO.LoginUserInfo info = mapper.getLoginUserInfo(params);

        //아이디가 적합한 객체가 존재할 경우 비번 체크
        if(info != null){
            // 비번이 매치되지 않으면 객체를 null로 바꾼다.
            if(!passwordEncoder.matches(params.getUserPw(),info.getUserPw())){
                info = null;
            }
        }
        return info;
    }


}
