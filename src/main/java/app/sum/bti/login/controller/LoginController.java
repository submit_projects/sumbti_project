package app.sum.bti.login.controller;

import app.sum.bti.login.service.LoginService;
import app.sum.bti.login.vo.LoginVO;
import app.sum.bti.security.ZRsaSecurity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.PrivateKey;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequiredArgsConstructor
@Slf4j
public class LoginController {

    private final LoginService loginService;
    
    
    //로그아웃처리
    @GetMapping("/login/out")
    public ModelAndView logOut(HttpServletRequest request) {
    	ModelAndView view = new ModelAndView();
    	
    	if(request.getSession().getAttribute("loginUserInfo") != null) {
    		request.getSession().invalidate();
    	}
    	
    	view.setViewName("views/logIn/logInPage");
    	return view;
    	
    }
    
    
    
    
    // 로그인 화면
    @GetMapping("/login")
    public ModelAndView login() {
        ModelAndView view = new ModelAndView();
        view.setViewName("views/logIn/logInPage");

        return view;
    }


    // public 키만들기  ajax
    // 키를생성해서 프론트에 넘겨주는 ajax콜의 응답형을 만듬!
    // 페이지가 리플레쉬 될때마다 키를 생성해서 저장
    @GetMapping("/get/keys")
    @ResponseBody     
    public Map<String,Object> getRsaPublicKey(HttpServletRequest request) {  // private키는 세션에 저장할것이므로 request 받음
        Map<String,Object> resultMap = new HashMap<String,Object>();

        try {
            //RSA 객체선언
            ZRsaSecurity rsa = new ZRsaSecurity();

            // 비공개키 객체 가져오기 (비공개키 생성)
            // PrivateKey -> 얘는 실제 인터페이스?
            PrivateKey privateKey = rsa.getPrivateKey();

            //만약 기존에 저장된 비공개키가 있으면 삭제(원래 안해도됌? 세션은 키가 중복되면 교체는 되지만 혹시나하고 ..)
            if(request.getSession().getAttribute("_rsaPrivateKey_") != null){
                request.getSession().removeAttribute("_rsaPrivateKey_");
            }

            //새로운 비공개키 저장
            request.getSession().setAttribute("_rsaPrivateKey_",privateKey);
            
            // id와 패스워드를 암호화 할 공개키 생성
            String publicKeyModules = rsa.getRsaPublicKeyModulus();
            String publicKeyExponent = rsa.getRsaPublicKeyExponent();

            //공개키를 리턴 map에저장
            resultMap.put("publicKeyModules",publicKeyModules);
            resultMap.put("publicKeyExponent",publicKeyExponent);

        }catch (Exception e){
            e.printStackTrace();
        }

        return resultMap;
    }


    // form에 입력된 정보와 DB에서 로그인 정보 비교 후 세션에 사용자 정보 저장
    @PostMapping("/checkLogin")
    @ResponseBody
    public Map<String, Object> checkUserInfo(@ModelAttribute LoginVO.LoginInfo loginInfo, HttpServletRequest request){
        Map<String, Object> resultMap = new HashMap<String, Object>();

        try {
            //RSA 객체 선언
            ZRsaSecurity rsa = new ZRsaSecurity();
            
            //비공개키 가져오기
            PrivateKey privateKey = (PrivateKey)request.getSession().getAttribute("_rsaPrivateKey_");

            //복호화
            String userId = rsa.decryptRSA(privateKey, loginInfo.getUserId());
            String userPw = rsa.decryptRSA(privateKey, loginInfo.getUserPw());

            //복호화 된 것으로 변경
            loginInfo.setUserId(userId);
            loginInfo.setUserPw(userPw);

            //사용자정보 가져오기
            LoginVO.LoginUserInfo loginUserInfo = loginService.getLoginUserInfo(loginInfo);

            if(loginUserInfo == null){
                throw new Exception("로그인실패");
            }else{
                //비공개키 삭제
                request.getSession().removeAttribute("_rsaPrivateKey_");
                resultMap.put("resultCode", 200);
                //세션에 사용자 정보 입력하기
                request.getSession().setAttribute("loginUserInfo",loginUserInfo);
            }

        } catch (Exception e) {
            // exception 발생시 에러코드 500
            resultMap.put("resultCode",500);
            e.printStackTrace();
        }

        return resultMap;
    }


    // 로그인 하지 않았을 시 동작할 메서드
    @GetMapping("/login/error")
    public ModelAndView errorLogin() {
        ModelAndView view = new ModelAndView();
        view.setViewName("views/logIn/error");

        return view;
    }
}
