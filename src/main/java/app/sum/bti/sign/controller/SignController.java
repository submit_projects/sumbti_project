package app.sum.bti.sign.controller;

import app.sum.bti.sign.service.SignService;
import app.sum.bti.sign.vo.SignVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequiredArgsConstructor
@Slf4j
public class SignController {

    private final SignService signService;


    @GetMapping("/signinGo1")
    public ModelAndView signInF() {
        ModelAndView view = new ModelAndView();
        view.setViewName("views/signIn/signIn");

        return view;
    }

    @GetMapping("/signinGo")
    public ModelAndView signInFirst() {
        ModelAndView view = new ModelAndView();
        view.setViewName("views/signIn/signIn-1");

        return view;
    }

    @GetMapping("/signinGo2")
    public ModelAndView signInSecond() {
        ModelAndView view = new ModelAndView();
        view.setViewName("views/signIn/signIn-2");

        return view;
    }

    @GetMapping("/selectMbtiGo")
    public ModelAndView signInThird() {
        ModelAndView view = new ModelAndView();
        view.setViewName("views/selectMbti/selectPage2");

        return view;
    }

    // 아이디 중복체크
    @PostMapping("/checkIdVal")
    @ResponseBody
    public Map<String, Object> checkSameId(@RequestParam(value = "checkId") String checkId) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        // 쿼리에 전달할 파라미터 만들기
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("checkId",checkId);

        try {
            int result = signService.checkEqualId(param);

            if(result > 0){
                // 중복 있음
                resultMap.put("resultCode",201);
            }else{
                // 중복없음
                resultMap.put("resultCode",200);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultMap;
    }

    // 닉네임 중복체크
    @PostMapping("/checkNickVal")
    @ResponseBody
    public Map<String, Object> checkSameNick(@RequestParam(value = "checkNick") String checkNick) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        // 쿼리에 전달할 파라미터 만들기
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("checkNick",checkNick);

        try {
            int result = signService.checkEqualNick(param);

            if(result > 0){
                // 중복 있음
                resultMap.put("resultCode",201);
            }else{
                // 중복없음
                resultMap.put("resultCode",200);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultMap;
    }



//signService.saveFile(InfoRequest.getProfilePicture());
    @PostMapping("/signIn/complete")
    @ResponseBody
    public Map<String, Object> nextSign3(@ModelAttribute SignVO.SignInfo signInfo) {
        Map<String, Object> resultMap = new HashMap<>();

       log.info("===============================",signInfo.getProfilePicture());

        try {
           int result =  signService.signCheck(signInfo);

           // 회원정보 저장완료 시
           if(result > 0){
               // 회원 사진정보 저장
               SignVO.Request request = new SignVO.Request();
                     request.setUserId(signInfo.getIdInput());
                     request.setProfilePicture(signInfo.getProfilePicture());

               int resultUp = signService.addPicture(request);

               // resultUp > 0 이면 최종 회원가입 완료
               if(resultUp > 0){
                   resultMap.put("resultCode",200);
               }else{
                   // 사진저장 실패시 회원가입 실패(저장된 회원정보를 지운다)
                   signService.deleteInfo(request.getUserId());
                   resultMap.put("resultCode",500);
               }
           }else{
               // 회원정보 저장 실패
               resultMap.put("resultCode",501);
           }

        }catch (Exception e){
            e.printStackTrace();
        }
        
        return resultMap;
    }
}
