package app.sum.bti.common;

import com.mortennobel.imagescaling.AdvancedResizeOp;
import com.mortennobel.imagescaling.MultiStepRescaleOp;
import groovyjarjarpicocli.CommandLine;
import org.springframework.beans.factory.annotation.Value;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.UUID;

// 이미지 리사이즈
public class Utils {

    public static String ResizeFile(int width, int height, File originFile, String imagePath){

        String thumbFileName = null;

        InputStream in = null;
        BufferedInputStream  buf  = null;

        try {
            if(originFile != null) {
                String originFileName = originFile.getName();
                String ext = originFileName.substring(originFileName.lastIndexOf(".") + 1);
                //uuid를 사용해서 이름 만들기
                String uuid = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 10);

                thumbFileName = uuid + "." + ext;

                in =  new  FileInputStream(originFile);
                buf =  new BufferedInputStream(in);

                //원본 이미지 파일 뜨기 (inputStream을 buffered에 넣어서 좀더 빠르게)
                BufferedImage originalImage = ImageIO.read(new BufferedInputStream( new FileInputStream( originFile)));
                //이미지 사이즈 줄이기 (라이브러리 2개필요)
                MultiStepRescaleOp scaleImage = new MultiStepRescaleOp(width, height);
                //마스킹(이미지 표면처리?)
                scaleImage.setUnsharpenMask(AdvancedResizeOp.UnsharpenMask.Soft);
                //리사이즈된 이미지 생성
                BufferedImage resizeImage = scaleImage.filter(originalImage,null);

                //리사이즈된 이미지 실제 파일로 만들기?
                //separator 는 os에 맞게 구분자를 만들어줌(직접만들지 않고)
                String thumbImagPath = imagePath + "thumb" + File.separator + thumbFileName;
                File scaledFile = new File(thumbImagPath);

                // 저장할 폴더 경로가 없다면 생성
                if(!scaledFile.getParentFile().exists()){
                    scaledFile.getParentFile().mkdirs();
                }

                //리사이즈한 파일을 실제 경로에 생성. 결과를 리턴해준다.
                boolean isWrite = ImageIO.write(resizeImage,ext,scaledFile);

                if(!isWrite){
                    throw new Exception("파일 리사이즈 오류");
                }
            }
        }catch (Exception e){
            thumbFileName = null;
            e.printStackTrace();
        }finally {

            try {
                if (buf != null) {
                    buf.close();
                }
                if(in  != null) {
                    in.close();
                }
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
        return thumbFileName;

    }

}

