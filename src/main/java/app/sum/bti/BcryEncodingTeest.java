package app.sum.bti;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BcryEncodingTeest {
    public static void main(String[] args) {

        //비크립트 암호화 객체
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String password = "12345";

        //암호화
        String encoded = encoder.encode(password);

        System.out.println(encoded);
        System.out.println(encoder.matches(password,encoded));
    }
}
